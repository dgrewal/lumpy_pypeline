import pypeliner
import pypeliner.managed as mgd
import tasks

def create_lumpy_workflow(lumpy_vcf, config, tumour_bam = None, tumour_bai = None, normal_bam = None, normal_bai = None):

	workflow = pypeliner.workflow.Workflow()

	if normal_bam:
		workflow.transform(
			name='run_samtools_view_normal',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'03:00'},
			func=tasks.run_samtools_view,
			args=(
				mgd.InputFile(normal_bam),
				mgd.TempOutputFile('normal.discordants.unsorted.bam'),
			),
		)

		workflow.transform(
			name='run_lumpy_extract_split_reads_bwamem_normal',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'05:00'},
			func=tasks.run_lumpy_extract_split_reads_bwamem,
			args=(
				mgd.InputFile(normal_bam),
				mgd.TempOutputFile('normal.splitters.unsorted.bam'),
				config,
			),
		)

		workflow.transform(
			name='run_samtools_sort_discordants_normal',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'01:00'},
			func=tasks.run_samtools_sort,
			args=(
				mgd.TempInputFile('normal.discordants.unsorted.bam'),
				mgd.TempOutputFile('normal.discordants.sorted.bam'),
			),
		)

		workflow.transform(
			name='run_samtools_sort_splitters_normal',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'01:00'},
			func=tasks.run_samtools_sort,
			args=(
				mgd.TempInputFile('normal.splitters.unsorted.bam'),
				mgd.TempOutputFile('normal.splitters.sorted.bam'),
			),
		)

	if tumour_bam:
		workflow.transform(
			name='run_samtools_view_tumour',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'03:00'},
			func=tasks.run_samtools_view,
			args=(
				mgd.InputFile(tumour_bam),
				mgd.TempOutputFile('tumour.discordants.unsorted.bam'),
			),
		)

		workflow.transform(
			name='run_lumpy_extract_split_reads_bwamem_tumour',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'07:00'},
			func=tasks.run_lumpy_extract_split_reads_bwamem,
			args=(
				mgd.InputFile(tumour_bam),
				mgd.TempOutputFile('tumour.splitters.unsorted.bam'),
				config,
			),
		)

		workflow.transform(
			name='run_samtools_sort_discordants_tumour',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'01:00'},
			func=tasks.run_samtools_sort,
			args=(
				mgd.TempInputFile('tumour.discordants.unsorted.bam'),
				mgd.TempOutputFile('tumour.discordants.sorted.bam'),
			),
		)

		workflow.transform(
			name='run_samtools_sort_splitters_tumour',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'01:00'},
			func=tasks.run_samtools_sort,
			args=(
				mgd.TempInputFile('tumour.splitters.unsorted.bam'),
				mgd.TempOutputFile('tumour.splitters.sorted.bam'),
			),
		)

	if tumour_bam and not normal_bam:
		workflow.transform(
			name='run_lumpyexpress_unpaired_tumour',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'04:00'},
			func=tasks.run_lumpyexpress,
			args=(
				mgd.OutputFile(lumpy_vcf),
				config,
			),
			kwargs={
				'tumour_bam': mgd.InputFile(tumour_bam),
				'tumour_discordants': mgd.TempInputFile('tumour.discordants.sorted.bam'),
				'tumour_splitters': mgd.TempInputFile('tumour.splitters.sorted.bam')
			}
		)
	elif not tumour_bam and normal_bam:
		workflow.transform(
			name='run_lumpyexpress_unpaired_normal',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'04:00'},
			func=tasks.run_lumpyexpress,
			args=(
				mgd.OutputFile(lumpy_vcf),
				config,
			),
			kwargs={
				'normal_bam': mgd.InputFile(normal_bam),
				'normal_discordants': mgd.TempInputFile('normal.discordants.sorted.bam'),
				'normal_splitters': mgd.TempInputFile('normal.splitters.sorted.bam')
			}
		)
	else:
		workflow.transform(
			name='run_lumpyexpress_paired',
			ctx={'mem': config['memory']['med'], 'pool_id': config['pools']['standard'], 'ncpus': 1,, 'walltime':'04:00'},
			func=tasks.run_lumpyexpress,
			args=(
				mgd.OutputFile(lumpy_vcf),
				config,
			),
			kwargs={
				'tumour_bam': mgd.InputFile(tumour_bam),
				'tumour_discordants': mgd.TempInputFile('tumour.discordants.sorted.bam'),
				'tumour_splitters': mgd.TempInputFile('tumour.splitters.sorted.bam'),
				'normal_bam': mgd.InputFile(normal_bam),
				'normal_discordants': mgd.TempInputFile('normal.discordants.sorted.bam'),
				'normal_splitters': mgd.TempInputFile('normal.splitters.sorted.bam')
			}
		)

	return workflow
