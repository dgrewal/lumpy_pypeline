import csv

import yaml
import pandas as pd
import pysam
import warnings


def read_bams_file(bams_file):

    bams = pd.read_csv(bams_file, dtype=str)

    for column in ('sample_id', 'tumour', 'normal',):
        if column not in bams.columns:
            raise Exception(
                'input bams_file should contain {}'.format(column))

    sample_ids = list(sorted(bams['sample_id'].unique()))

    if bams.duplicated(['sample_id']).any():
        raise Exception('input bams_file with duplicate sample_id')

    tumour_files = dict()
    normal_files = dict()

    for _, row in bams.iterrows():
        tumour_files[row['sample_id']] = None if pd.isna(row['tumour']) else row['tumour'].strip()
        normal_files[row['sample_id']] = None if pd.isna(row['normal']) else row['normal'].strip()

    if not all(tumour_files.values()):
        tumour_files = None
    if not all(normal_files.values()):
        normal_files = None

    if not tumour_files and not normal_files:
        raise Exception('Tumour and normal bams can\'t both be unspecified')

    return tumour_files, normal_files, sample_ids


def load_config(args):
    try:
        with open(args['config_file']) as infile:
            config = yaml.load(infile)

    except IOError:
        raise Exception(
            'Unable to open config file: {0}'.format(
                args['config_file']))
    return config
