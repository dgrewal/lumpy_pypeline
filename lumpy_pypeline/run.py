import os
import argparse
import utils
import pypeliner
import pypeliner.managed as mgd
from workflows import lumpy_pypeline


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    pypeliner.app.add_arguments(parser)

    parser.add_argument('--bams_file',
                        required=True,
                        help='''Path to input bam CSV.''')
    parser.add_argument('--config_file',
                        required=True,
                        help='''Path to yaml config file.''')
    parser.add_argument('--out_dir',
                        required=True,
                        help='''Path to output files.''')

    args = vars(parser.parse_args())

    return args


def main():
    args = parse_args()
    config = utils.load_config(args)

    pyp = pypeliner.app.Pypeline(config=args)

    tumour_bam, normal_bam, sample_ids = utils.read_bams_file(args['bams_file'])

    workflow = pypeliner.workflow.Workflow()

    workflow.setobj(
        obj=mgd.OutputChunks('sample_id'),
        value=sample_ids,
    )

    if tumour_bam:
        tumour_bai = dict([(sample_id, str(tumour_bam[sample_id]) + '.bai')
                             for sample_id in sample_ids])
    if normal_bam:
        normal_bai = dict([(sample_id, str(normal_bam[sample_id]) + '.bai')
                             for sample_id in sample_ids])

    output_dir = os.path.join(args['out_dir'], '{sample_id}')

    lumpy_vcf = os.path.join(output_dir, 'lumpy.vcf')

    if tumour_bam and not normal_bam:
        # Unpaired with only tumour
        workflow.subworkflow(
            name='lumpy_pypeline_unpaired_tumour',
            func=lumpy_pypeline.create_lumpy_workflow,
            axes=('sample_id',),
            args=(
                mgd.OutputFile(lumpy_vcf, 'sample_id'),
                config,
            ),
            kwargs={
                'tumour_bam': mgd.InputFile('tumour_bam', 'sample_id', fnames=tumour_bam),
                'tumour_bai': mgd.InputFile('tumour_bai', 'sample_id', fnames=tumour_bai),
            }
        )
    elif not tumour_bam and normal_bam:
        # Unpaired with only normal
        workflow.subworkflow(
            name='lumpy_pypeline_unpaired_normal',
            func=lumpy_pypeline.create_lumpy_workflow,
            axes=('sample_id',),
            args=(
                mgd.OutputFile(lumpy_vcf, 'sample_id'),
                config,
            ),
            kwargs={
                'normal_bam': mgd.InputFile('normal_bam', 'sample_id', fnames=normal_bam),
                'normal_bai': mgd.InputFile('normal_bai', 'sample_id', fnames=normal_bai),
            }
        )
    elif tumour_bam and normal_bam:
        # Paired
        workflow.subworkflow(
            name='lumpy_pypeline_unpaired_normal',
            func=lumpy_pypeline.create_lumpy_workflow,
            axes=('sample_id',),
            args=(
                mgd.OutputFile(lumpy_vcf, 'sample_id'),
                config,
            ),
            kwargs={
                'tumour_bam': mgd.InputFile('tumour_bam', 'sample_id', fnames=tumour_bam),
                'tumour_bai': mgd.InputFile('tumour_bai', 'sample_id', fnames=tumour_bai),
                'normal_bam': mgd.InputFile('normal_bam', 'sample_id', fnames=normal_bam),
                'normal_bai': mgd.InputFile('normal_bai', 'sample_id', fnames=normal_bai),
            }
        )
    else:
        raise Exception('Input CSV cannot be empty.')

    pyp.run(workflow)

if __name__ == '__main__':
    main()
