from setuptools import setup, find_packages
import versioneer
import os

extra_files = ['scripts/*.sh', ]

setup(
    name='lumpy_pypeline',
    packages=find_packages(),
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description='Lumpy pipeline',
    author='',
    author_email='',
    entry_points={'console_scripts': ['lumpy_pypeline = lumpy_pypeline.run:main']},
    package_data={'': extra_files},
)
